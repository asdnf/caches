package com.example.demo.storage;

import com.example.demo.storage.wrapper.IDirectory;
import com.example.demo.storage.wrapper.IFile;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class LRUStorageTest {

    @Mock
    StorageFactory factory;

    @Mock
    IDirectory persistedDirectory;

    LRUStorage storage;

    List<IFile> files;

    @Mock
    IFile file;

    @Before
    public void beforeEachTest() {
        when(factory.createLinkedList()).thenReturn(new LinkedList<>());
        when(factory.createEntry()).thenReturn(new Entry());
        when(factory.createObjectMapper()).thenReturn(new ObjectMapper());
        when(factory.createLinkedList(any())).thenCallRealMethod();
        files = new ArrayList<>();
        IntStream.range(1, 8).forEach(i -> files.add(new IFile() {
            @Override
            public String getContents() {
                return "{\"id\":" + i + ", \"data\":\"pepe\", \"addressedIndex\":0, \"addressedCounter\":0}";
            }

            @Override
            public void setContents(String contents) {

            }

            @Override
            public void delete() {
            }

            @Override
            public String getAbsolutePath() {
                return "path";
            }

            @Override
            public String getName() {
                return "7";
            }

            @Override
            public void persist() {

            }

            @Override
            public void load() {

            }
        }));
        files.add(file);
        when(persistedDirectory.getContents()).thenReturn(new LinkedList<>(files));
        storage = new LRUStorage(5, 5, persistedDirectory, factory);
    }

    @Test
    public void getInMemoryCapacity() {
        assert storage.getInMemoryCapacity() == 5;
    }

    @Test
    public void getPersistedCapacity() {
        assert storage.getPersistedCapacity() == 5;
    }

    @Test
    public void getPersistedEntriesCount() {
        assert storage.getPersistedEntriesCount() == 8;
    }

    @Test
    public void getInMemoryEntriesCount() {
        storage.save("333");
        storage.save("333");
        storage.save("333");
        assert storage.getInMemoryEntriesCount() == 3;
    }

    @Test
    public void clearStorageDir() {
        // cleared in constructor
        verify(file, atLeastOnce()).delete();
    }

    @Test
    public void organizeSavedEntryInMemory() {
        when(factory.createFile(any(), anyString())).thenReturn(file);
        storage.save("23");
        storage.organizeSavedEntryInMemory();
        verify(file, atLeastOnce()).persist();
    }

    @Test
    public void organizeSavedEntryOnDisk() {
    }

    @Test
    public void save() {
    }

    @Test
    public void update() {
        doCallRealMethod().when(factory).createEntry();
        storage.save("333"); //date: 1 frequency: 1
        storage.save("333"); //      2            1
        storage.save("333"); //      3            1
        storage.update(1, "1212"); //4         2
        assert storage.get(1).getAddressedIndex() == 5; //5  3
        assert storage.get(1).getAddressedCounter() == 4; //5   4
    }

    @Test
    public void get() {
    }

    @Test
    public void listInMemoryItems() {
        storage.save("");
        storage.save("");
        storage.save("");
        assert storage.listInMemoryItems().size() == 3;
    }

    @Test
    public void listOnDiskItems() {
        files.remove(files.size() - 1);
        when(persistedDirectory.getContents()).thenReturn(new LinkedList<>(files));
        assert storage.listOnDiskItems().size() == 7;

    }

    @Test
    public void savePersistentEntry() {
        Entry entry = new Entry();
        entry.setId(1);
        when(persistedDirectory.getItemByName(anyString())).thenReturn(file);
        storage.savePersistentEntry(entry);
        verify(file, atLeastOnce()).persist();
    }

    @Test
    public void getPersistentEntry() {
    }

    public void testLiftPersistentEntry() {
        //storage.liftPersistentEntry();
    }
}