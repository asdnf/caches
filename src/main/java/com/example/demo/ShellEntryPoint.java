package com.example.demo;

import com.example.demo.storage.Entry;
import com.example.demo.storage.LFUStorage;
import com.example.demo.storage.LRUStorage;
import com.example.demo.storage.StorageFactory;
import com.example.demo.storage.wrapper.fsimpl.DirectoryWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.List;

@ShellComponent
public class ShellEntryPoint {

    LRUStorage lruStorage;
    LFUStorage lfuStorage;

    @Autowired
    StorageFactory storageFactory;

    ObjectMapper objectMapper = new ObjectMapper();

    @ShellMethod("Create LRU cache instance")
    // Example: lru-create --mem-cap 5 --disk-cap 5 --store-dir "C:\\Users\\44586\\Desktop\\123"
    public String lruCreate(
            @ShellOption Integer memCap,
            @ShellOption Integer diskCap,
            @ShellOption String storeDir) {

        DirectoryWrapper dw = new DirectoryWrapper(storeDir);
        lruStorage = new LRUStorage(memCap, diskCap, dw, storageFactory);

        return String.format("LRU storage created.\r\nmemory capacity: %d\r\ndisk capacity: %d\r\ndirectory: %s",
                memCap, diskCap, dw.getAbsolutePath());
    }

    @ShellMethod("Save data into LRU cache")
    // Example: lru-save --data "232323"
    public String lruSave(@ShellOption String data) {
        if (lruStorage == null) {
            return "LRU storage must be created first";
        }

        int id = lruStorage.save(data);

        return String.format("Data saved with id %d", id);
    }

    @ShellMethod("Update data into LRU cache")
    // Example: lru-update --id 2 --data "111"
    public String lruUpdate(@ShellOption Integer id,
                            @ShellOption String data) {
        if (lruStorage == null) {
            return "LRU storage must be created first";
        }

        lruStorage.update(id, data);

        return String.format("Data updated");
    }

    @ShellMethod("Get data from LRU cache")
    // Example: lru-get --id 3
    public String lruGet(@ShellOption Integer id) throws JsonProcessingException {
        if (lruStorage == null) {
            return "LRU storage must be created first";
        }

        Entry entry = lruStorage.get(id);
        String entryStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(entry);
        return entryStr;
    }

    @ShellMethod("Get data from LRU cache")
    // Example: lru-ls-mem
    public String lruLsMem() throws JsonProcessingException {
        if (lruStorage == null) {
            return "LRU storage must be created first";
        }
        List<Entry> entries = lruStorage.listInMemoryItems();
        String entriesStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(entries);
        return entriesStr;
    }

    @ShellMethod("Get data from LRU cache")
    // Example: lru-ls-disk
    public String lruLsDisk() throws JsonProcessingException {
        if (lruStorage == null) {
            return "LRU storage must be created first";
        }
        List<Entry> entries = lruStorage.listOnDiskItems();
        String entriesStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(entries);
        return entriesStr;
    }





    @ShellMethod("Create LFU cache instance")
    // Example: lfu-create --mem-cap 5 --disk-cap 5 --store-dir "C:\\Users\\44586\\Desktop\\123"
    public String lfuCreate(
            @ShellOption Integer memCap,
            @ShellOption Integer diskCap,
            @ShellOption String storeDir) {

        DirectoryWrapper dw = new DirectoryWrapper(storeDir);
        lfuStorage = new LFUStorage(memCap, diskCap, dw, storageFactory);

        return String.format("LFU storage created.\r\nmemory capacity: %d\r\ndisk capacity: %d\r\ndirectory: %s",
                memCap, diskCap, dw.getAbsolutePath());
    }

    @ShellMethod("Save data into LFU cache")
    // Example: lfu-save --data "232323"
    public String lfuSave(@ShellOption String data) {
        if (lfuStorage == null) {
            return "LFU storage must be created first";
        }

        int id = lfuStorage.save(data);

        return String.format("Data saved with id %d", id);
    }

    @ShellMethod("Update data into LFU cache")
    // Example: lru-update --id 2 --data "111"
    public String lfuUpdate(@ShellOption Integer id,
                            @ShellOption String data) {
        if (lfuStorage == null) {
            return "LFU storage must be created first";
        }

        lfuStorage.update(id, data);

        return String.format("Data updated");
    }

    @ShellMethod("Get data from LFU cache")
    // Example: lfu-get --id 3
    public String lfuGet(@ShellOption Integer id) throws JsonProcessingException {
        if (lfuStorage == null) {
            return "LFU storage must be created first";
        }
        Entry entry = lfuStorage.get(id);
        String entryStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(entry);
        return entryStr;
    }

    @ShellMethod("Get data from LFU cache")
    // Example: lfu-ls-mem
    public String lfuLsMem() throws JsonProcessingException {
        if (lfuStorage == null) {
            return "LFU storage must be created first";
        }
        List<Entry> entries = lfuStorage.listInMemoryItems();
        String entriesStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(entries);
        return entriesStr;
    }

    @ShellMethod("Get data from LFU cache")
    // Example: lfu-ls-disk
    public String lfuLsDisk() throws JsonProcessingException {
        if (lfuStorage == null) {
            return "LFU storage must be created first";
        }
        List<Entry> entries = lfuStorage.listOnDiskItems();
        String entriesStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(entries);
        return entriesStr;
    }
}
