package com.example.demo.storage;

import com.example.demo.storage.wrapper.IDirectory;
import com.example.demo.storage.wrapper.IFile;
import com.example.demo.storage.wrapper.fsimpl.FileWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Component
public class StorageFactory {

    public Entry createEntry() {
        return new Entry();
    }

    public LinkedList<Entry> createLinkedList() {
        return new LinkedList<>();
    }

    public LinkedList<Entry> createLinkedList(List<Entry> list) {
        return new LinkedList<>(list);
    }

    public HashMap<Integer, Entry> createHashMap() {
        return new HashMap<>();
    }

    public IFile createFile(IDirectory persistingDirectory, String name) {
        return new FileWrapper(persistingDirectory.getAbsolutePath(), name);
    }

    public ObjectMapper createObjectMapper() {
        return new ObjectMapper();
    }
}
