package com.example.demo.storage.wrapper;

public interface IFile extends IFileSystemItem {

    String getContents();

    void setContents(String contents);

    void delete();

}
