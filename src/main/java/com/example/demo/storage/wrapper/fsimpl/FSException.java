package com.example.demo.storage.wrapper.fsimpl;

public class FSException extends RuntimeException {
    public FSException(String message, Throwable cause) {
        super(message, cause);
    }

    public FSException(String message) {
        super(message);
    }
}
