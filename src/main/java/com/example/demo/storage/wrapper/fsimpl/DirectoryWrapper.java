package com.example.demo.storage.wrapper.fsimpl;

import com.example.demo.storage.wrapper.IDirectory;
import com.example.demo.storage.wrapper.IFileSystemItem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DirectoryWrapper implements IDirectory {

    final String directoryPath;
    final Path path;

    public DirectoryWrapper(String directoryPath) {
        this.directoryPath = directoryPath;
        this.path = Paths.get(directoryPath);
    }

    protected static IFileSystemItem pathToItem(Path path) {
        if (Files.isDirectory(path)) {
            return new DirectoryWrapper(path.toString());
        }
        if (Files.isRegularFile(path)) {
            return new FileWrapper(path.toString());
        }
        return null;
    }

    @Override
    public Iterable<IFileSystemItem> getContents() {
        try (Stream<Path> itemList = Files.list(path)) {
            return itemList.map(DirectoryWrapper::pathToItem).collect(Collectors.toList());
        } catch (IOException e) {
            throw new FSException("Unable to collect directory item list", e);
        }
    }

    @Override
    public IFileSystemItem getItemByName(String name) {
        try (Stream<Path> itemStream = Files.find(path, 1, (a, b) -> a.getFileName().toString().equals(name))) {
            Path found = itemStream.findFirst().orElse(null);
            if (found != null) {
                return pathToItem(found);
            }
        } catch (IOException e) {
            throw new FSException("Unable to find the item by name", e);
        }
        return null;
    }

    @Override
    public String getAbsolutePath() {
        return path.toString();
    }

    @Override
    public String getName() {
        return path.getFileName().toString();
    }

    @Override
    public void persist() {
        try {
            Files.createDirectories(path);
        } catch (IOException e) {
            throw new FSException("Unable to create directory", e);
        }
    }

    @Override
    public void load() {
        if (!Files.exists(path) || !Files.isDirectory(path)) {
            throw new FSException("The directory does not exist");
        }
    }

    @Override
    public String toString() {
        return "DirectoryWrapper{" +
                "directoryPath='" + directoryPath + '\'' +
                '}';
    }
}
