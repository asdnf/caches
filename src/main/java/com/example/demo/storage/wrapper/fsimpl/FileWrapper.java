package com.example.demo.storage.wrapper.fsimpl;

import com.example.demo.storage.wrapper.IFile;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileWrapper implements IFile {

    final String directoryPath;
    final Path path;
    BufferedReader contentStream;
    ByteArrayOutputStream dataOutputBuffer;

    public FileWrapper(String directoryPath, String filename) {
        this.directoryPath = directoryPath + File.separator + filename;
        this.path = Paths.get(this.directoryPath);
    }

    public FileWrapper(String directoryPath) {
        this.directoryPath = directoryPath;
        this.path = Paths.get(directoryPath);
    }

    @Override
    public String getContents() {
        try {
            char[] buff = new char[1024];
            int n;
            StringBuilder sb = new StringBuilder();
            while ((n = contentStream.read(buff)) >= 0) {
                if (n > 0) {
                    sb.append(new String(buff, 0, n));
                }
            }
            return sb.toString();
        } catch (IOException e) {
            throw new FSException("Unable to read contents", e);
        }
    }

    @Override
    public void setContents(String contents) {
        dataOutputBuffer = new ByteArrayOutputStream();
        try {
            dataOutputBuffer.write(contents.getBytes());
        } catch (IOException e) {
            throw new FSException("Unable to set contents", e);
        }
    }

    @Override
    public String getAbsolutePath() {
        return path.toString();
    }

    @Override
    public String getName() {
        return path.getFileName().toString();
    }

    @Override
    public void persist() {
        try {
            Files.write(path, dataOutputBuffer.toByteArray());
        } catch (IOException e) {
            throw new FSException("Unable to write contents to file", e);
        }
    }

    @Override
    public void load() {
        try {
            contentStream = Files.newBufferedReader(path);
        } catch (IOException e) {
            throw new FSException("Unable to open the file for reading", e);
        }
    }

    @Override
    public void delete() {
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new FSException("Unable to delete the file", e);
        }
    }
}
