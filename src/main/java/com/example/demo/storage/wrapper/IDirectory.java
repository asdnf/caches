package com.example.demo.storage.wrapper;

public interface IDirectory extends IFileSystemItem {

    Iterable<IFileSystemItem> getContents();

    IFileSystemItem getItemByName(String name);

}
