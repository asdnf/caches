package com.example.demo.storage.wrapper;

public interface IFileSystemItem {

    String getAbsolutePath();
    String getName();
    void persist();
    void load();

}
