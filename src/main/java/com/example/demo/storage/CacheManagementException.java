package com.example.demo.storage;

public class CacheManagementException extends RuntimeException {
    public CacheManagementException() {
        super();
    }

    public CacheManagementException(String message) {
        super(message);
    }

    public CacheManagementException(String message, Throwable cause) {
        super(message, cause);
    }

    public CacheManagementException(Throwable cause) {
        super(cause);
    }
}
