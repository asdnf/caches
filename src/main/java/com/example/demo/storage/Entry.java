package com.example.demo.storage;

public class Entry {

    Integer id;
    String data;
    Integer addressedIndex;
    Integer addressedCounter;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getAddressedIndex() {
        return addressedIndex;
    }

    public void setAddressedIndex(Integer addressedIndex) {
        this.addressedIndex = addressedIndex;
    }

    public Integer getAddressedCounter() {
        return addressedCounter;
    }

    public void setAddressedCounter(Integer addressedCounter) {
        this.addressedCounter = addressedCounter;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "id=" + id +
                ", data='" + data + '\'' +
                ", addressedSequence=" + addressedIndex +
                ", addressedCounter=" + addressedCounter +
                '}';
    }
}
