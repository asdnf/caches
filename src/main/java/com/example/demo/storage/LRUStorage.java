package com.example.demo.storage;

import com.example.demo.storage.wrapper.IDirectory;
import com.example.demo.storage.wrapper.IFile;
import com.example.demo.storage.wrapper.IFileSystemItem;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LRUStorage {

    LinkedList<Entry> entryList;
    Map<Integer, Entry> entryMap;

    IDirectory persistingDirectory;

    int inMemoryCapacity;
    int persistedCapacity;

    int idSequence = 0;
    int accessSequence = 0;

    ObjectMapper mapper;

    StorageFactory factory;

    public LRUStorage(Integer inMemoryCapacity, Integer persistedCapacity,
                      IDirectory persistingDirectory, StorageFactory factory) {
        this.factory = factory;
        this.inMemoryCapacity = inMemoryCapacity;
        this.persistedCapacity = persistedCapacity;
        this.persistingDirectory = persistingDirectory;
        this.entryList = factory.createLinkedList();
        this.entryMap = factory.createHashMap();
        this.mapper = factory.createObjectMapper();
        persistingDirectory.persist();
        clearStorageDir();
    }

    protected LinkedList<Entry> getEntryList() {
        return entryList;
    }

    protected Map<Integer, Entry> getEntryMap() {
        return entryMap;
    }

    public int getInMemoryCapacity() {
        return inMemoryCapacity;
    }

    public int getPersistedCapacity() {
        return persistedCapacity;
    }

    public int getPersistedEntriesCount() {
        int count = 0;
        for (IFileSystemItem fsi : persistingDirectory.getContents()) {
            count++;
        }
        return count;
    }

    public int getInMemoryEntriesCount() {
        return entryList.size();
    }

    public void clearStorageDir() {
        for (IFileSystemItem fsi : persistingDirectory.getContents()) {
            if (fsi instanceof IFile) {
                ((IFile) fsi).delete();
            }
        }
    }

    protected void organizeSavedEntryInMemory() {
        Entry lastEntry = entryList.removeLast();
        int id = lastEntry.getId();
        entryMap.remove(id);
        IFile file = factory.createFile(persistingDirectory, String.valueOf(id));
        try {
            file.setContents(mapper.writeValueAsString(lastEntry));
        } catch (JsonProcessingException e) {
            throw new CacheManagementException("Unable to serialize entry", e);
        }
        file.persist();
    }

    protected void organizeSavedEntryOnDisk() {
        Entry leastEntry = null;
        IFile leastFile = null;
        for (IFileSystemItem fsi : persistingDirectory.getContents()) {
            if (fsi instanceof IFile) {
                fsi.load();
                String fileContent = ((IFile) fsi).getContents();
                Entry fileEntry = null;
                try {
                    fileEntry = mapper.readValue(fileContent, Entry.class);
                } catch (JsonProcessingException e) {
                    throw new CacheManagementException("Unable to deserialize entry", e);
                }

                if (leastEntry == null) {
                    leastEntry = fileEntry;
                    leastFile = (IFile) fsi;
                }

                if (leastEntry.getAddressedIndex() > fileEntry.getAddressedIndex()) { // LRU comparator
                    leastEntry = fileEntry;
                    leastFile = (IFile) fsi;
                }
            }
        }
        if (leastFile != null) {
            leastFile.delete();
        }
    }

    public int save(String data) {
        Entry e = factory.createEntry();
        e.setData(data);
        e.setId(++idSequence);
        e.setAddressedCounter(1);
        e.setAddressedIndex(++accessSequence);
        entryList.push(e);
        entryMap.put(idSequence, e);

        if (getInMemoryEntriesCount() > getInMemoryCapacity()) {
            organizeSavedEntryInMemory();

            if (getPersistedEntriesCount() > getPersistedCapacity()) {
                organizeSavedEntryOnDisk();
            }
        }

        return idSequence;
    }

    public void update(Integer id, String data) {
        Entry entry = entryMap.get(id);
        boolean persistent = false;
        if (entry == null) {
            entry = getPersistentEntry(id);
            if (entry == null) {
                return;
            }
            persistent = true;
        }

        entry.setData(data);
        entry.setAddressedIndex(++accessSequence);
        int addressedCounter = entry.getAddressedCounter();
        entry.setAddressedCounter(addressedCounter + 1);

        if (persistent) {
            savePersistentEntry(entry);
        } else {
            entryList.remove(entry);
            entryList.push(entry);
        }
    }

    protected void liftPersistentEntry(Entry storedEntry) {
        if (storedEntry.getAddressedIndex() > entryList.getLast().getAddressedIndex()) {
            Entry memEntry = entryList.removeLast();
            entryMap.remove(memEntry.getId());

            IFileSystemItem fsi = persistingDirectory.getItemByName(String.valueOf(storedEntry.getId()));
            ((IFile) fsi).delete();

            savePersistentEntry(memEntry);
            entryList.add(storedEntry);

        }
    }

    public Entry get(Integer id) {
        Entry entry = entryMap.get(id);
        if (entry == null) {
            Entry persistentEntry = getPersistentEntry(id);
            if (persistentEntry != null) {
                Integer addressedCount = persistentEntry.getAddressedCounter();
                persistentEntry.setAddressedIndex(++accessSequence);
                persistentEntry.setAddressedCounter(addressedCount + 1);
                liftPersistentEntry(persistentEntry);
                entryList.push(entryList.removeLast());
                return persistentEntry;
            }
            return null;
        }

        Integer addressedCount = entry.getAddressedCounter();
        entry.setAddressedIndex(++accessSequence);
        entry.setAddressedCounter(addressedCount + 1);

        entryList.remove(entry);
        entryList.push(entry);

        return entry;
    }

    public List<Entry> listInMemoryItems() {
        return factory.createLinkedList(entryList);
    }

    public List<Entry> listOnDiskItems() {
        List<Entry> output = factory.createLinkedList();
        for (IFileSystemItem fsi : persistingDirectory.getContents()) {
            if (fsi instanceof IFile) {
                fsi.load();
                String entryStr = ((IFile) fsi).getContents();
                try {
                    Entry entry = mapper.readValue(entryStr, Entry.class);
                    output.add(entry);
                } catch (JsonProcessingException e) {
                    throw new CacheManagementException("Unable to deserialize entry", e);
                }
            }
        }
        return output;
    }

    protected void savePersistentEntry(Entry entry) {
        IFileSystemItem fsi = persistingDirectory.getItemByName(String.valueOf(entry.getId()));
        IFile file = (IFile) fsi;
        try {
            if (file == null) {
                file = factory.createFile(persistingDirectory, String.valueOf(entry.getId()));
            }
            file.setContents(mapper.writeValueAsString(entry));
            file.persist();
        } catch (JsonProcessingException e) {
            throw new CacheManagementException("Unable to serialize entry", e);
        }
    }

    protected Entry getPersistentEntry(Integer id) {
        IFileSystemItem fsi = persistingDirectory.getItemByName(String.valueOf(id));
        if (fsi instanceof IFile) {
            IFile file = (IFile) fsi;
            file.load();
            String entryStr = file.getContents();
            try {
                Entry entry = mapper.readValue(entryStr, Entry.class);
                return entry;
            } catch (JsonProcessingException e) {
                throw new CacheManagementException("Unable to deserialize entry", e);
            }
        }
        return null;
    }
}
